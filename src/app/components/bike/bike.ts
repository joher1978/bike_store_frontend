export interface IBike {
    id?: number;
    model: string;
    serial: string;
    price: number;
}

export class Bike implements IBike {
    id?: number;
    model: string;
    serial: string;
    price: number;
}
